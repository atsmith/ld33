﻿using UnityEngine;
using System.Collections;

public class ToMainTimed : MonoBehaviour 
{
    [SerializeField]
    private int ToLevel = 0;
    [SerializeField]
    private float TimeToWait = 5;

    private float TimeWaited = 0;
	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () {
        TimeWaited += Time.deltaTime;
        if(TimeWaited > TimeToWait)
        {
            Application.LoadLevel(ToLevel);
        }
	}
}
