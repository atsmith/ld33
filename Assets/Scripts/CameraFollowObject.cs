﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Camera))]
public class CameraFollowObject : MonoBehaviour {

    [SerializeField]
    private GameObject ObjectToFollow = null;

    private Vector3 ObjectPosition = new Vector3();

	// Use this for initialization
	void Start () {
	    if(null == ObjectToFollow)
        {
            Debug.LogError("ObjectToFollow cannot be null.");
        }
	}
	
	// Update is called once per frame
	void Update () {
        ObjectPosition = ObjectToFollow.transform.position;
        ObjectPosition.z = transform.position.z;

        transform.position = ObjectPosition;
	}
}
