﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator), typeof(Animation))]
public class MonsterAnimate : MonoBehaviour
{

    private Animator anim;
    private MonsterMovement mm;

	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
        mm = transform.parent.GetComponent<MonsterMovement>();
	}
	
    int Hash(string name)
    {
        return Animator.StringToHash(name);
    }

	protected void FixedUpdate () 
    {
        anim.SetBool(Hash(Constants.Standing), mm.IsIdle);
        anim.SetBool(Hash(Constants.Walking), mm.IsWalking);
        anim.SetBool(Hash(Constants.Jumping), mm.IsJumping);
        anim.SetBool(Hash(Constants.InAir), mm.IsInAir);
        anim.SetBool(Hash(Constants.Landing), mm.IsLanding);
        anim.SetBool(Hash(Constants.Falling), mm.IsFalling);
	}
}
