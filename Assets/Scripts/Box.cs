﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Box : MonoBehaviour
{

    private Vector3 StartPosition;
    private Rigidbody2D Body;

	// Use this for initialization
	void Start () {
        Body = GetComponent<Rigidbody2D>();
        StartPosition = transform.position;
	}
	
    void OnTriggerEnter2D(Collider2D Col)
    {
        if(Col.gameObject.tag.Equals("Death"))
        {
            transform.position = StartPosition;
            Body.velocity = Vector3.zero;
        }
    }
}
