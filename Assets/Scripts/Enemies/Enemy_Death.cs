﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Enemy_Death : MonoBehaviour
{

    [SerializeField]
    private LayerMask RayAcknowledge;

    private BoxCollider2D Box;
    private Animator anim;
    private Vector2 Origin;
    private BasicEnemy be;

    private int AttackingHash;

	// Use this for initialization
	void Start ()
    {
        Box = GetComponent<BoxCollider2D>();
        if(!Box.isTrigger)
        {
            Box.isTrigger = true;
        }
        Origin = Vector2.zero;
        if(null == transform.parent)
        {
            anim = null;
            be = null;
        }
        else
        {
            anim = transform.parent.gameObject.GetComponent<Animator>();
            be = transform.parent.gameObject.GetComponent<BasicEnemy>();
            Transform trans = transform.parent;
            Origin.x = trans.position.x;
            Origin.y = trans.position.y;
            AttackingHash = Animator.StringToHash(Constants.Attacking);
        }
	}

    void FixedUpdate()
    {
        if(Hit())
        {
            anim.SetBool(AttackingHash, true);
        }
        else
        {
            if (null != anim)
            {
                anim.SetBool(AttackingHash, false);
            }
        }
    }

    bool Hit()
    {
        if (null == transform.parent)
        {
            return false;
        }

        Transform trans = transform.parent;
        Origin.x = trans.position.x;
        Origin.y = trans.position.y;
        RaycastHit2D hit = Physics2D.Raycast(Origin, trans.right * be.Direction, 0.35f, RayAcknowledge.value);
        if (null != hit.collider)
        {
            if (hit.collider.gameObject.tag.Equals(Constants.Player))
            {
                return true;
            }
        }
        return false;
    }
}
