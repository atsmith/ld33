﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Animation))]
public class BasicEnemy : MonoBehaviour
{

    [SerializeField]
    private float LeftBound = -5;
    [SerializeField]
    private float RightBound = 5;

    [SerializeField]
    private float MovementSpeed = 3;

    [SerializeField]
    private bool FacingRight = true;

    private Vector3 Velocity;
    private float _Direction;
    private Rigidbody2D Body;
    private Animator anim;
    private int AttackingHash;

    public float Direction { get { return _Direction; } }

	// Use this for initialization
	void Start ()
    {
        Body = GetComponent<Rigidbody2D>();
        _Direction = (FacingRight) ? 1 : -1;
        Vector3 scale = transform.localScale;
        scale.x *= (FacingRight) ? -1 : 1;
        transform.localScale = scale;
        Velocity = Vector3.zero;
        anim = GetComponent<Animator>();
        AttackingHash = Animator.StringToHash(Constants.Attacking);
	}
	
	void FixedUpdate ()
    {
        if (anim.GetBool(AttackingHash))
        {
            Velocity.x = 0;
        }
        else
        {
            Velocity.x = MovementSpeed * Direction;
        }
        Velocity.y = Body.velocity.y;
        Body.velocity = Velocity;
        if((transform.position.x <= LeftBound && -1 == Direction) || (transform.position.x >= RightBound && 1 == Direction))
        {
            _Direction *= -1;
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
        
	}
}
