﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator), typeof(Animation))]
public class Button : MonoBehaviour
{

    [SerializeField]
    private List<string> GameObjectTag = new List<string>(0);

    private bool _pressed;
    private Animator anim;
    private BoxCollider2D Box;
    private int PressedCount;

    public bool Pressed { get { return _pressed; } }

	// Use this for initialization
	void Start ()
    {
        Box = GetComponent<BoxCollider2D>();
        if(!Box.isTrigger)
        {
            Box.isTrigger = true;
        }
        anim = GetComponent<Animator>();
        PressedCount = 0;
	}

    void FixedUpdate()
    {
        anim.SetBool(Animator.StringToHash("Pressed"), Pressed);
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        for (int TagIndex = 0; TagIndex < GameObjectTag.Count; ++TagIndex)
        {
            if (Col.gameObject.tag.Equals(GameObjectTag[TagIndex]))
            {
                _pressed = true;
                PressedCount++;
            }
        }
    }

    void OnTriggerExit2D(Collider2D Col)
    {
        for (int TagIndex = 0; TagIndex < GameObjectTag.Count; ++TagIndex)
        {
            if (Col.gameObject.tag.Equals(GameObjectTag[TagIndex]))
            {
                PressedCount--;
            }
        }

        if(0 == PressedCount)
        {
            // do this here, since there could still be
            // an object left in the trigger that is still
            // valid
            _pressed = false;
        }
    }
}
