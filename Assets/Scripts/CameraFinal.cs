﻿using UnityEngine;
using System.Collections;

public class CameraFinal : MonoBehaviour
{
    [SerializeField]
    private Vector3 EndingPosition;
    [SerializeField]
    private float Speed;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        float Change = Speed * Time.deltaTime;

        Vector3 Position = transform.position;

        if(Position.y > EndingPosition.y)
        {
            Position.y -= Change;
        }

        transform.position = Position;
	}
}
