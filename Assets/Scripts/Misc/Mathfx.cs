﻿using UnityEngine;
using System.Collections;

public static class Mathfx
{
    public static bool IsZero(float Value, float Tolerance = 0.000001f)
    {
        return Mathf.Abs(Value) <= Tolerance;
    }

    public static bool IsEqual(float A, float B, float Tolerance = 0.000001f)
    {
        return (Mathf.Abs(A + Tolerance) >= B);
    }

    public static int Sign(float Value)
    {
        return (Value < 0) ? -1 : 1;
    }
}
