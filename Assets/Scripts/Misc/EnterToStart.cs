﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CInputComponent))]
public class EnterToStart : MonoBehaviour
{
    [SerializeField]
    private int LevelToGoTo = -1;
    private CInputComponent InputComponent;

	// Use this for initialization 
	void Start ()
    {
        InputComponent = GetComponent<CInputComponent>();
        InputComponent.BindAction(Constants.Enter, InputEvent.Released, OnEnter);
	}

    void OnEnter()
    {
        Application.LoadLevel(LevelToGoTo);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
