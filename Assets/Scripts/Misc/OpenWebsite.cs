﻿using UnityEngine;
using System.Collections;

public class OpenWebsite : MonoBehaviour
{
    [SerializeField]
    private string Website = string.Empty;

    [SerializeField]
    private Color MouseOverColor = Color.blue;

    [SerializeField]
    private Color MouseExitColor = Color.white;

    [SerializeField]
    private TextMesh Text = null;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	void OnMouseDown()
    {
        if(string.Empty != Website)
        {
            Application.OpenURL(Website);
        }
    }

    void OnMouseOver()
    {
        Text.color = MouseOverColor;
    }

    void OnMouseExit()
    {
        Text.color = MouseExitColor;
    }
}
