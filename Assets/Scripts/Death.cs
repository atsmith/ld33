﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EdgeCollider2D))]
public class Death : MonoBehaviour
{
    private EdgeCollider2D Edge;

	// Use this for initialization
	void Start ()
    {
        Edge = GetComponent<EdgeCollider2D>();
        if(!Edge.isTrigger)
        {
            Edge.isTrigger = true;
        }
	}
}
