﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum InputEvent
{
    Pressed,
    Released,
    Repeat
}

delegate void AxisCallback(float val);
delegate void ActionCallback();
delegate void ActionAxisCallback(bool positive);
[AddComponentMenu("Input/Input Component")]
class CInputComponent : MonoBehaviour
{
    private Dictionary<string, List<AxisCallback>> _axisBindings;
    private Dictionary<string, Dictionary<InputEvent, List<ActionCallback>>> _actionBindings;
    private Dictionary<string, List<ActionAxisCallback>> _actionAxisBindings;

    static bool InputInitialized = false;

    public static void Initialize()
    {
        if (!InputInitialized)
        {
            cInput.Init();

            SetKey(Constants.MoveLeft, Keys.A, Keys.Xbox1LStickLeft);
            SetKey(Constants.MoveRight, Keys.D, Keys.Xbox1LStickRight);
            SetKey(Constants.Jump, Keys.Space, Keys.Xbox1A);
            SetKey(Constants.Enter, Keys.Enter, Keys.Xbox1Start);
            SetKey(Constants.Quit, Keys.Escape, Keys.Xbox1Back);

            SetAxis(Constants.MoveHorizontal, Constants.MoveLeft, Constants.MoveRight);
            InputInitialized = true;
        }
    }

    void Awake()
    {
        _axisBindings = new Dictionary<string, List<AxisCallback>>();
        _actionBindings = new Dictionary<string, Dictionary<InputEvent, List<ActionCallback>>>();
        _actionAxisBindings = new Dictionary<string, List<ActionAxisCallback>>();
        Initialize();
    }

    void Update()
    {
        // Axis bindings
        foreach (KeyValuePair<string, List<AxisCallback>> pair in _axisBindings)
        {
            string axisName = pair.Key;
            foreach (AxisCallback callback in pair.Value)
            {
                callback(GetAxis(axisName));
            }
        }

        // Action axis bindings
        foreach (KeyValuePair<string, List<ActionAxisCallback>> pair in _actionAxisBindings)
        {
            string axisName = pair.Key;
            foreach (ActionAxisCallback callback in pair.Value)
            {
                if (!Mathfx.IsZero(GetAxis(axisName)))
                {
                    if (cInput.GetButtonDown(axisName))
                    {
                        callback(GetAxis(axisName) > 0);
                    }
                }
            }
        }

        // Action bindings
        foreach (KeyValuePair<string, Dictionary<InputEvent, List<ActionCallback>>> pair in _actionBindings)
        {
            string keyName = pair.Key;
            foreach (KeyValuePair<InputEvent, List<ActionCallback>> inputEvent in pair.Value)
            {
                InputEvent type = inputEvent.Key;
                foreach (ActionCallback callback in inputEvent.Value)
                {
                    switch (type)
                    {
                        case InputEvent.Pressed:
                            if (cInput.GetButtonDown(keyName))
                            {
                                callback();
                            }
                            break;
                        case InputEvent.Repeat:
                            if (cInput.GetButton(keyName))
                            {
                                callback();
                            }
                            break;
                        case InputEvent.Released:
                            if (cInput.GetButtonUp(keyName))
                            {
                                callback();
                            }
                            break;

                    }
                }
            }
        }
    }

    public bool HasAxis(string axisName)
    {
        return cInput.IsAxisDefined(axisName);
    }

    public bool HasKey(string keyName)
    {
        return cInput.IsKeyDefined(keyName);
    }

    public float GetAxis(string axisName)
    {
        if (!HasAxis(axisName))
        {
            return 0f;
        }

        return cInput.GetAxis(axisName);
    }

    public float GetAxisRaw(string axisName)
    {
        if (!HasAxis(axisName))
        {
            return 0f;
        }

        return cInput.GetAxisRaw(axisName);
    }

    public bool BindAxis(string axisName, AxisCallback callback)
    {
        if (!HasAxis(axisName) || null == callback)
        {
            Debug.LogError("Did not bind axis.");
            return false;
        }

        List<AxisCallback> callbacks;

        // get the list of callbacks if the axis has been added
        // before
        if (_axisBindings.ContainsKey(axisName))
        {
            if (!_axisBindings.TryGetValue(axisName, out callbacks))
            {
                // we failed to get the list, but the axis has been
                // added before, so we won't try to add the axis a
                // second time, return failure
                return false;
            }
        }
        else
        {
            callbacks = new List<AxisCallback>();
            _axisBindings.Add(axisName, callbacks);
        }

        callbacks.Add(callback);

        return true;
    }

    public bool BindAction(string axisName, ActionAxisCallback callback)
    {
        if (!HasAxis(axisName) || null == callback)
        {
            Debug.LogError(string.Format("Axis does not exist.  {0}", axisName));
            return false;
        }

        List<ActionAxisCallback> callbacks;

        if (_actionAxisBindings.ContainsKey(axisName))
        {
            if (!_actionAxisBindings.TryGetValue(axisName, out callbacks))
            {
                return false;
            }
        }
        else
        {
            callbacks = new List<ActionAxisCallback>();
            _actionAxisBindings.Add(axisName, callbacks);
        }

        callbacks.Add(callback);

        return true;
    }

    public bool BindAction(string keyName, InputEvent type, ActionCallback callback)
    {
        if (!HasKey(keyName) || null == callback)
        {
            Debug.Log("Failed to bind key to " + keyName);
            return false;
        }

        // we do somthing similar to the thing we did in BindAxis here, 
        // only with _actionBindings instead
        Dictionary<InputEvent, List<ActionCallback>> inputValue = new Dictionary<InputEvent, List<ActionCallback>>();
        List<ActionCallback> callbacks = new List<ActionCallback>();

        // success is used because the axis could exist, but not the
        // input event that is being requested
        bool success = false;
        // axisExists is used because the axis could exist, but not the
        // input event that is being requested.  If this is the case,
        // then we should not add it again.
        bool axisExists = false;

        if (_actionBindings.ContainsKey(keyName))
        {
            axisExists = true;
            if (!_actionBindings.TryGetValue(keyName, out inputValue))
            {
                return false;
            }

            if (inputValue.ContainsKey(type))
            {
                if (!inputValue.TryGetValue(type, out callbacks))
                {
                    return false;
                }
                success = true;
            }
        }

        if (axisExists && !success)
        {
            callbacks = new List<ActionCallback>();
            inputValue.Add(type, callbacks);
            success = true;
        }

        if (!axisExists && !success)
        {
            callbacks = new List<ActionCallback>();
            inputValue = new Dictionary<InputEvent, List<ActionCallback>>();
            inputValue.Add(type, callbacks);
            _actionBindings.Add(keyName, inputValue);
        }

        callbacks.Add(callback);
        return true;
    }

    public static int SetKey(string action, string primary)
    {
        return cInput.SetKey(action, primary);
    }

    public static int SetKey(string action, string primary, string secondary)
    {
        return cInput.SetKey(action, primary, secondary);
    }

    public static int SetKey(string action, string primary, string secondary, string primaryModifier)
    {
        return cInput.SetKey(action, primary, secondary, primaryModifier);
    }

    public static int SetKey(string action, string primary, string secondary, string primaryModifier, string secondaryModifier)
    {
        return cInput.SetKey(action, primary, secondary, primaryModifier, secondaryModifier);
    }

    public static int SetAxis(string axisName, string input)
    {
        return cInput.SetAxis(axisName, input);
    }

    public static int SetAxis(string axisName, string input, float sensitivity)
    {
        return cInput.SetAxis(axisName, input, sensitivity);
    }

    public static int SetAxis(string axisName, string negativeInput, string positiveInput)
    {
        return cInput.SetAxis(axisName, negativeInput, positiveInput);
    }
}