﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Warp : MonoBehaviour
{
    [SerializeField]
    private int NextLevel = -1;

    private BoxCollider2D Box;

	// Use this for initialization
	void Start ()
    {
	    if(-1 == NextLevel)
        {
            NextLevel = Application.loadedLevel;
        }

        Box = GetComponent<BoxCollider2D>();
        if(!Box.isTrigger)
        {
            Box.isTrigger = true;
        }
	}
	
    void OnTriggerEnter2D(Collider2D Col)
    {
        if(Col.gameObject.tag.Equals("Player"))
        {
            //TODO: Play flashing light thingy
            Application.LoadLevel(NextLevel);
        }
    }
}
