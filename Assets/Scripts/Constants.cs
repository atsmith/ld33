﻿using UnityEngine;
using System.Collections;

public static class Constants
{
    public const string MoveLeft       = "Move Left";
    public const string MoveRight      = "Move Right";
    public const string MoveHorizontal = "Horizontal Movement";
    public const string Jump           = "Jump";
    public const string Enter          = "Enter";
    public const string Quit           = "QUIT_GAME";


    public const string Attacking      = "Attacking";
    public const string Standing       = "Idle";
    public const string Jumping        = "Jump";
    public const string InAir          = "InAir";
    public const string Landing        = "Landing";
    public const string Walking        = "Walk";
    public const string Falling        = "Fall";
    public const string HasHelmet      = "HelmetHead";
    public const string HasChestPlate  = "HelmetChest";
    public const string HasLeggings    = "Leggings";
    public const string HasSword       = "Sword";
    public const string Player         = "Player";
}
