﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[RequireComponent(typeof(Collider2D))]
public class DrawAllCollidersOnObject : MonoBehaviour
{

#if UNITY_EDITOR

    [SerializeField]
    private List<Collider2D> Colliders = new List<Collider2D>();

	// Use this for initialization
	void Start ()
    {

	}

    void DrawBox(BoxCollider2D Box)
    {
        Bounds bound = Box.bounds;
        Vector3[] Points = new Vector3[4];
        for (int i = 0; i < Points.Length; ++i)
        {
            Points[i].x = transform.position.x + bound.center.x;
            Points[i].y = transform.position.y + bound.center.y;
            Points[i].z = 0;
        }

        Points[0].x -= bound.extents.x;
        Points[0].y += bound.extents.y;
        Points[1].x -= bound.extents.x;
        Points[1].y -= bound.extents.y;
        Points[2].x += bound.extents.x;
        Points[2].y -= bound.extents.y;
        Points[3].x += bound.extents.x;
        Points[3].y += bound.extents.y;

        Gizmos.DrawLine(Points[0], Points[1]);
        Gizmos.DrawLine(Points[1], Points[2]);
        Gizmos.DrawLine(Points[2], Points[3]);
        Gizmos.DrawLine(Points[3], Points[0]);
    }

    void DrawCircle(CircleCollider2D Circle)
    {
        Vector3 Scale = transform.lossyScale;
        Scale.z = 0;
        Vector3 Center = transform.position + (Vector3)Circle.offset;
        Center.x *= Scale.x;
        Center.y *= Scale.y;
        float RadiusScale = Scale.magnitude * 0.727f;
        UnityEditor.Handles.DrawWireDisc(Center, Vector3.back, Circle.radius * RadiusScale );
    }
	
	void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Gizmos.color = Color.blue;

        for(int i = 0; i < Colliders.Count; ++i)
        {
            if (Colliders[i].gameObject != gameObject)
            {
                Debug.LogError("Collider " + Colliders[i].GetType().Name + " (Element " + i + ") is not a part of " + gameObject.name);
            }
            if(!Colliders[i])
            {
                Debug.LogError("Collider " + i + " not set to an instance.");
                return;
            }
            System.Type Type = Colliders[i].GetType();
            if(Type == typeof(BoxCollider2D))
            {
                DrawBox((BoxCollider2D)Colliders[i]);
            }
            if(Type == typeof(CircleCollider2D))
            {
                DrawCircle((CircleCollider2D)Colliders[i]);
            }
            
        }
    }

#endif
}