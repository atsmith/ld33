﻿using UnityEngine;
using System.Collections;

enum EFacingDirection
{
    FD_Left,
    FD_Right
}

[RequireComponent (typeof(CInputComponent))]
[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(BoxCollider2D))]
[RequireComponent (typeof(CircleCollider2D))]
[RequireComponent (typeof(AudioSource))]
public class MonsterMovement : MonoBehaviour {

    [SerializeField]
    [Range(0, 100)]
    private float MovementSpeed = 5;

    [SerializeField]
    private LayerMask GroundMask;

    [SerializeField]
    [Range(0, 20)]
    private float JumpForce = 3;

    [SerializeField]
    private float JumpPauseTime = 0.3f;
    [SerializeField]
    private float LandingDistance = 0.3f;

    [SerializeField]
    private bool hasHelmet = false;
    [SerializeField]
    private bool hasChestPlate = false;
    [SerializeField]
    private bool hasLeggings = false;
    [SerializeField]
    private bool hasSword = false;

    [SerializeField]
    private GameObject Naked = null;
    [SerializeField]
    private GameObject Helmet = null;
    [SerializeField]
    private GameObject Chest = null;
    [SerializeField]
    private GameObject Leggings = null;

    [SerializeField]
    private AudioClip WalkClip = null;
    [SerializeField]
    private AudioClip JumpClip = null;

    private AudioSource Source;

    private EFacingDirection FacingDirection;

    private CInputComponent InputComponent;
    private Rigidbody2D Body;

    private Vector2 Velocity = new Vector2();
    private Vector2 Position2d = new Vector2();
    private bool isJumping = false;
    private bool InAir = false;
    private bool isLanding = false;
    private bool isFalling = false;
    private bool isDying = false;
    private bool isDead = false;

    private float TimeToPauseJump;

    private BoxCollider2D Box;

    private bool ImmediateJump = false;

    // used for determining if the character is still touching another object
    // that would continue to keep it 'grounded'.
    private int Collisions;

    // private values made public

    public bool HasHelmet { get { return hasHelmet; } }
    public bool HasChestPlate { get { return hasChestPlate; } }
    public bool HasLeggings { get { return hasLeggings; } }
    public bool HasSword { get { return hasSword; } }

    public bool IsIdle { get { return Vector2.zero.Equals(Velocity); } }
    public bool IsWalking { get { return !Mathfx.IsZero(Velocity.x); } }
    public bool IsJumping { get { return isJumping; } }
    public bool IsInAir { get { return InAir; } }
    public bool IsLanding { get { return isLanding; } }
    public bool IsFalling { get { return isFalling; } }
    public bool IsDying { get { return isDying; } }
    public bool IsDead { get { return isDead; } }

	// Use this for initialization
	void Start ()
    {
        InputComponent = GetComponent<CInputComponent>();
        Box = GetComponent<BoxCollider2D>();
        Body = GetComponent<Rigidbody2D>();
        Source = GetComponent<AudioSource>();

        TimeToPauseJump = 0;

        Collisions = 0;

        FacingDirection = EFacingDirection.FD_Right;

        SetupMonsterInput();
        SetupMonsterImage();
	}

    void SetupMonsterInput()
    {
        InputComponent.BindAxis(Constants.MoveHorizontal, this.MoveHorizontal);
        InputComponent.BindAction(Constants.Jump, InputEvent.Repeat, this.Jump);
        InputComponent.BindAction(Constants.Quit, InputEvent.Released, this.QuitGame);
    }

    void QuitGame()
    {
        Application.Quit();
    }

    void SetupMonsterImage()
    {
        if(!HasHelmet && !HasChestPlate && !HasLeggings)
        {
            Naked.SetActive(true);
        }
        Helmet.SetActive(HasHelmet);
        Chest.SetActive(HasChestPlate);
        Leggings.SetActive(HasLeggings);
    }

    void FixedUpdate()
    {
        TimeToPauseJump += Time.fixedDeltaTime;
        
        SetFlip();
        SetJumping();
        SetInAir();

        if(!Mathfx.IsZero(Velocity.x) && !IsJumping && !(Source.clip == WalkClip && Source.isPlaying))
        {
            Source.clip = WalkClip;
            Source.Play();
        }
        else if(Mathfx.IsZero(Velocity.x) && Source.clip == WalkClip)
        {
            Source.Stop();
        }
        
        Body.velocity = Velocity;
        Position2d.x = transform.position.x;
        Position2d.y = transform.position.y;
    }

    private void SetJumping()
    {
        if (!ImmediateJump || (ImmediateJump && TimeToPauseJump < JumpPauseTime))
        {
            Vector3 Temp = Velocity;
            Velocity = Body.velocity;
            Velocity.x = Temp.x;
        }
        else if (ImmediateJump && TimeToPauseJump >= JumpPauseTime)
        {
            ImmediateJump = false;
            isJumping = true;
            Velocity.y = JumpForce;
            Source.clip = JumpClip;
            Source.Play();
        }

        //anim.SetBool(JumpingHash, IsJumping);
    }

    private void SetFlip()
    {
        if (!Mathfx.IsZero(Velocity.x))
        {
            int Sign = Mathfx.Sign(Velocity.x);
            if ((EFacingDirection.FD_Right == FacingDirection && -1 == Sign) ||
                (EFacingDirection.FD_Left == FacingDirection && 1 == Sign))
            {
                Vector3 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
                if (EFacingDirection.FD_Left == FacingDirection)
                {
                    FacingDirection = EFacingDirection.FD_Right;
                }
                else
                {
                    FacingDirection = EFacingDirection.FD_Left;
                }
            }
        }
    }

    private void SetInAir()
    {
        InAir = false;
        isLanding = false;
        if (!ImmediateJump && isJumping)
        {
            if (Velocity.y <= 0)
            {
                RaycastHit2D hit;
                Vector2 Direction = Vector2.up * -1;
                hit = Physics2D.Raycast(Position2d, Direction, Mathf.Infinity, GroundMask.value);
                if (null != hit.collider)
                {
                    if(Mathf.Abs(hit.distance) <= LandingDistance)
                    {
                        InAir = false;
                        isLanding = true;
                    }
                    else
                    {
                        InAir = true;
                    }
                }
                else
                {
                    InAir = true;
                }
            }
            else
            {
                InAir = true;
            }

        }

        //anim.SetBool(InAirHash, IsInAir);
        //anim.SetBool(LandingHash, IsLanding);
    }

    private bool IsBoxCollider(ContactPoint2D[] Contacts)
    {
        for (int i = 0; i < Contacts.Length; ++i)
        {
            if (Contacts[i].collider == Box)
            {
                return true;
            }
        }

        return false;
    }

    void OnCollisionEnter2D(Collision2D Col)
    {
        ContactPoint2D[] Contacts = Col.contacts;
        if ((Col.gameObject.tag.Equals("Block") || Col.gameObject.tag.Equals("Box")) && !IsBoxCollider(Contacts))
        {
            Collisions++;
            isJumping = false;
        }
        if(Col.gameObject.tag.Equals("Platform") && !IsBoxCollider(Col.contacts))
        {
            Collisions++;
            transform.parent = Col.gameObject.transform;
            isJumping = false;
        }
    }

    void OnCollisionExit2D(Collision2D Col)
    {
        if((Col.gameObject.tag.Equals("Block") || Col.gameObject.tag.Equals("Box")) && !IsBoxCollider(Col.contacts))
        {
            Collisions--;
        }
        if (Col.gameObject.tag.Equals("Platform") && !IsBoxCollider(Col.contacts))
        {
            Collisions--;
            transform.parent = null;
        }

        if(0 == Collisions && !IsBoxCollider(Col.contacts))
        {
            isJumping = true;
        }
    }

    void OnTriggerEnter2D(Collider2D Col)
    {
        bool died = false;
        if(Col.gameObject.tag.Equals("Death"))
        {
            died = true;
        }
        else if(Col.gameObject.tag.Equals("Enemy_Death"))
        {
            //TODO: Play death animation and wait for it to finish
            died = true;
        }

        if(died)
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void MoveHorizontal(float Value)
    {
        if(ImmediateJump)
        {
            Value = 0;
        }
        Velocity.x = MovementSpeed * Value;
    }

    void Jump()
    {
        if(isJumping || ImmediateJump)
        {
            return;
        }
        TimeToPauseJump = 0;
        ImmediateJump = true;
        isJumping = true;
    }
}
