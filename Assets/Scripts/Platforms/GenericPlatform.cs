﻿using UnityEngine;
using System.Collections;

public class GenericPlatform : MonoBehaviour
{
    [SerializeField]
    protected float DistanceToTravel = 5;

    [SerializeField]
    [Range(0.000001f, 100)]
    protected float TimeBetweenChange = 3;

    protected float TimeSinceChange;
    protected Vector3 StartingPosition, EndingPosition;

	// Use this for initialization
	protected void Start ()
    {
        StartingPosition = transform.position;
        EndingPosition = transform.position;
        TimeSinceChange = 0;
	}
	
	protected void FixedUpdate ()
    {
        transform.position = Vector3.Lerp(StartingPosition, EndingPosition, TimeSinceChange / TimeBetweenChange);
        TimeSinceChange += Time.fixedDeltaTime;
        if (Mathf.Abs(Vector3.Distance(transform.position, EndingPosition)) <= 0.000001f)
        {
            Vector3 temp = EndingPosition;
            EndingPosition = StartingPosition;
            StartingPosition = temp;
            TimeSinceChange = 0;
        }
	}
}
