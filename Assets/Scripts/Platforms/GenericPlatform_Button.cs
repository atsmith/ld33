﻿using UnityEngine;
using System.Collections;

public class GenericPlatform_Button : GenericPlatform
{
    [SerializeField]
    private Button PressedButton = null;

	// Use this for initialization
	new protected void Start ()
    {
#if UNITY_EDITOR
	    if(null == PressedButton)
        {
            Debug.LogError("PressedButton cannot be null.");
        }
#endif
        base.Start();
	}
	
	new protected void FixedUpdate () {
	    if(PressedButton.Pressed)
        {
            base.FixedUpdate();
        }
	}
}
