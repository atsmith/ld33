﻿using UnityEngine;
using System.Collections;

public class ContinuousPlatform_Button : MonoBehaviour
{
    [SerializeField]
    private Button DropButton = null;

    private bool _active;

    protected bool Active { get { return _active; } }

	// Use this for initialization
	protected void Start ()
    {
#if UNITY_EDITOR
        if (null == DropButton)
        {
            Debug.LogError("DropButton cannot be null.");
        }
#endif
        _active = false;
	}
	
	protected void FixedUpdate ()
    {
	    if(!Active && DropButton.Pressed)
        {
            _active = true;
        }
	}
}
