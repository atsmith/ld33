﻿using UnityEngine;
using System.Collections;

public class DrawBridge : ContinuousPlatform_Button
{
    [SerializeField]
    private bool RotateRight = false;

    private bool Dropped;
    private int RotationDirection;
    private float Rotated;

	// Use this for initialization
	new void Start ()
    {
        base.Start();
        Dropped = false;
        RotationDirection = (RotateRight) ? -1 : 1;
        Rotated = 0;
	}
	
	new void FixedUpdate ()
    {
        base.FixedUpdate();
        if(Active && !Dropped)
        {
            float RotateFrame = 90 * RotationDirection * Time.fixedDeltaTime;
            Rotated += RotateFrame;
            transform.Rotate(transform.forward * RotateFrame);

            if(Mathfx.IsEqual(Mathf.Abs(Rotated), 90))
            {
                Dropped = true;
            }
        }
	}
}
