﻿using UnityEngine;
using System.Collections;

public class Gate : ContinuousPlatform_Button
{
    [SerializeField]
    [Range(1, 10)]
    private float Height = 2;
    [SerializeField]
    [Range(0.000001f, 5)]
    private float TimeToRise = 3;

    private float TimeSinceStart;
    private Vector3 StartingPosition, EndingPosition;


	// Use this for initialization
	new void Start ()
    {
        base.Start();
        TimeSinceStart = 0;
        StartingPosition = EndingPosition = transform.position;
        EndingPosition.y += Height;
	}
	
	new void FixedUpdate ()
    {
        base.FixedUpdate();
        if (Active && !(TimeSinceStart > TimeToRise))
        {
            transform.position = Vector3.Lerp(StartingPosition, EndingPosition, (TimeSinceStart / TimeToRise));
            TimeSinceStart += Time.fixedDeltaTime;
        }
	}
}
